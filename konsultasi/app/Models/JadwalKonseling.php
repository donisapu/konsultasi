<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalKonseling extends Model
{
    use HasFactory;
    protected $table = 'jadwal_konselings';
    protected $fillable = ['id','id_pasien','id_psikolog','tanggal_konseling','jam_konseling','status_konseling'];

}
