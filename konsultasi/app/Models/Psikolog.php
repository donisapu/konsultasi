<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Psikolog extends Model
{
    use HasFactory;
    protected $table = 'psikologs';
    protected $fillable = ['nama','username','password','email','nomor_telepon','tahun_lulus','asal_universitas','tempat_praktik','nomor_str','file_str','foto'];
}
