<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Pasien;
use App\Models\Psikolog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Models\RiwayatKonseling;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.dashboard',['title'=> "Dashboard"]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function list_psikolog()
    {
        $get = Psikolog::where('status','Menunggu')->get();
        return view('admin.list_psikolog',['title'=>"Verifikasi Psikolog",'psikolog'=>$get]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function verifikasi($id)
    {
        $get = Psikolog::where('id',$id)->get();
        return view('admin.verifikasi',['title'=>"Verifikasi",'psikolog'=>$get]);
    }

    /**
     * Display the specified resource.
     */
    public function terima($id,Request $request)
    {
        $get = Psikolog::where('id',$id)->first();
        $get->status = 'Diterima';
        $get->save();
        Session::flash('success', 'Berhasil menerima Psikolog');
        return redirect('/admin/list_psikolog');
    }

    public function tolak($id,Request $request)
    {
        $get = Psikolog::where('id',$id)->first();
        $get->status = 'Ditolak';
        $get->save();
        Session::flash('success', 'Berhasil menolak Psikolog');
        return redirect('/admin/list_psikolog');
    }

    public function daftar_user()
    {
        $pasien = Pasien::all();
        $psikolog = Psikolog::where('status','diterima')->get();
        return view('admin.daftar_user',['title'=>"Daftar User",'pasien'=>$pasien,'psikolog'=>$psikolog]);
    }

    public function profil_pasien($id)
    {
        $get = Pasien::where('id',$id)->get();
        return view('admin.profil_pasien',['title'=>"Profil Pasien",'pasien'=>$get]);
    }

    public function profil_psikolog($id)
    {
        $get = Psikolog::where('id',$id)->get();
        return view('admin.profil_psikolog',['title'=>"Profil Psikolog",'psikolog'=>$get]);
    }

    public function edit_psikolog($id)
    {
        $get = Psikolog::where('id',$id)->get();
        return view('admin.edit_psikolog',['title'=>"Edit Psikolog",'psikolog'=>$get]);
    }

    public function update_psikolog($id,Request $request)
    {
        $data = Psikolog::find($id);
        $data->nama = $request->nama;
        $data->username = $request->username;
        $data->email = $request->email;
        if(request('password')){
            $data->password = Hash::make($request->password);
        }
        $data->nomor_telepon = $request->nomor_telepon;
        $data->tahun_lulus = $request->tahun_lulus;
        $data->asal_universitas = $request->asal_universitas;
        $data->tempat_praktik = $request->tempat_praktik;
        $data->nomor_str = $request->nomor_str;
        $foto = null;
        if (request('foto')) {
            if ($data->foto && file_exists(public_path('/psikolog/foto/' . $data->foto))) {
                $image_path = public_path('/psikolog/foto/') . $data->foto;
                unlink($image_path);
                $foto = request('foto');
                $directory = public_path('/psikolog/foto/');
                $namefile = time() . $foto->getClientOriginalName();
                $foto->move($directory, $namefile);
                $data->foto = $namefile;
            } else {
                $foto = request('foto');
                $directory = public_path('/psikolog/foto');
                $namefile = time() . $foto->getClientOriginalName();
                $foto->move($directory, $namefile);
                $data->foto = $namefile;
            }
        }
        $file_str = null;
        if (request('file_str')) {
            if ($data->file_str && file_exists(public_path('/psikolog/str/' . $data->file_str))) {
                $image_path = public_path('/psikolog/str/') . $data->file_str;
                unlink($image_path);
                $file_str = request('file_str');
                $directory = public_path('/psikolog/str/');
                $namefile = time() . $file_str->getClientOriginalName();
                $file_str->move($directory, $namefile);
                $data->file_str = $namefile;
            } else {
                $file_str = request('file_str');
                $directory = public_path('/psikolog/str');
                $namefile = time() . $file_str->getClientOriginalName();
                $file_str->move($directory, $namefile);
                $data->file_str = $namefile;
            }
        }
        $data->save();
        Session::flash('success', 'Data Psikolog berhasil diupdate');
        return redirect('/admin/profil_psikolog/'.$id);
    }

    public function edit_pasien($id)
    {
        $get = Pasien::where('id',$id)->get();
        return view('admin.edit_pasien',['title'=>"Edit Pasien",'pasien'=>$get]);
    }

    public function update_pasien($id,Request $request)
    {
        $data = Pasien::find($id);
        $data->nama = $request->nama;
        $data->username = $request->username;
        if(request('password')){
            $data->password = Hash::make($request->password);
        }
        $data->save();
        Session::flash('success', 'Data Pasien berhasil diupdate');
        return redirect('/admin/profil_pasien/'.$id);
    }

    public function riwayat_konseling()
    {
        $get = RiwayatKonseling::join('jadwal_konselings', 'riwayat_konselings.id_jadwal_konseling', '=', 'jadwal_konselings.id')
            ->join('psikologs', 'psikologs.id', '=', 'jadwal_konselings.id_psikolog')
            ->join('pasiens', 'pasiens.id', '=', 'jadwal_konselings.id_pasien')
            ->select('riwayat_konselings.*', 'jadwal_konselings.*', 'psikologs.nama as nama_psikolog', 'pasiens.nama as nama_pasien','psikologs.nomor_str')
            ->get();
        return view('admin.riwayat_konseling',['title'=>"Riwayat Konseling",'riwayat'=>$get]);
    }

    public function hapus_pasien($id)
    {
        $pasien = Pasien::find($id);
        $pasien->delete();
        return redirect('/admin/user');
    }
    public function hapus_psikolog($id)
    {
        $psikolog = Psikolog::find($id);
        if ($psikolog->foto && file_exists(public_path('/psikolog/foto/' . $psikolog->foto))) {
            $image_path = public_path('/psikolog/foto/') . $psikolog->foto;
            unlink($image_path);
        }
        if ($psikolog->file_str && file_exists(public_path('/psikolog/str/' . $psikolog->file_str))) {
            $image_path = public_path('/psikolog/str/') . $psikolog->file_str;
            unlink($image_path);
        }
        $psikolog->delete();
        return redirect('/admin/user');
    }

}
