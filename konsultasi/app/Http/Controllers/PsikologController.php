<?php

namespace App\Http\Controllers;

use App\Models\Psikolog;
use Illuminate\Http\Request;
use App\Models\JadwalKonseling;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;

class PsikologController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $id_psikolog = session()->get('id');
        $get = JadwalKonseling::where('id_psikolog',$id_psikolog)->count();
        return view('psikolog.dashboard',['title'=> "Dashboard",'jadwal'=>$get]);
    }

    // PROFIL

    public function profil()
    {
        $id = session()->get('id');
        $get = Psikolog::where('id',$id)->get();
        return view('psikolog.profil',['title'=>"Profil",'profil'=>$get]);
    }

    public function detail_profil($id)
    {
        $get = psikolog::where('id',$id)->get();
        return view('psikolog.detail_profil',['title'=>"Detail Profil",'profil'=>$get]);
    }

    public function edit_profil($id)
    {
        $get = psikolog::where('id',$id)->get();
        return view('psikolog.edit_profil',['title'=>"Edit Profil",'profil'=>$get]);
    }

    public function update_profil($id,Request $request)
    {
        $data = psikolog::find($id);
        Validator::make($request->all(), [
            'nama' => 'required',
        ])->validate();
        $data->nama = $request->nama;
        $foto = null;
        if (request('foto')) {
            if ($data->foto && file_exists(public_path('/psikolog/foto/' . $data->foto))) {
                $image_path = public_path('/psikolog/foto/') . $data->foto;
                unlink($image_path);
                $foto = request('foto');
                $directory = public_path('/psikolog/foto/');
                $namefile = time() . $foto->getClientOriginalName();
                $foto->move($directory, $namefile);
                $data->foto = $namefile;
            } else {
                $foto = request('foto');
                $directory = public_path('/psikolog/foto');
                $namefile = time() . $foto->getClientOriginalName();
                $foto->move($directory, $namefile);
                $data->foto = $namefile;
            }
        }
        $data->save();
        Session::flash('success', 'Profil berhasil diupdate');
        return redirect('/psikolog/detail_profil/'.$id);
    }

    // PROFIL
}
