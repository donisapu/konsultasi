<?php

namespace App\Http\Controllers;

use App\Models\JadwalKonseling;
use App\Http\Requests\StoreJadwalKonselingRequest;
use App\Http\Requests\UpdateJadwalKonselingRequest;

class JadwalKonselingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index_pasien()
    {
        $id = session()->get('id');
        $get = JadwalKonseling::join('psikologs', 'jadwal_konselings.id_psikolog', '=', 'psikologs.id')
        ->where('id_pasien', $id)
        ->whereDate('tanggal_konseling', '>=', now()->toDateString())
        ->select('jadwal_konselings.id as id_jadwal', 'psikologs.*', 'jadwal_konselings.*')
        ->get();

        return view('pasien.jadwal_konseling',['title'=>"Konseling Yang Akan Datang",'jadwal'=>$get]);
    }

    public function index_psikolog()
    {
        $id = session()->get('id');
        $get = JadwalKonseling::where('id_psikolog', $id)
        ->whereDate('tanggal_konseling', '>=', now()->toDateString())
        ->get();
        return view('psikolog.jadwal_konseling',['title'=>"Konseling Yang Akan Datang",'jadwal'=>$get]);
    }

    public function detail_jadwal($id)
    {
        $get = JadwalKonseling::join('psikologs','jadwal_konselings.id_psikolog','=','psikologs.id')
        ->where('jadwal_konselings.id',$id)
        ->get();
        $jadwal = $id;
        return view('pasien.detail_jadwal',['title'=>"Detail Konseling",'jadwal'=>$get,'id_jadwal'=>$jadwal]);
    }
}
