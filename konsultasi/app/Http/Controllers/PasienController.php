<?php

namespace App\Http\Controllers;

use App\Models\Pasien;
use App\Models\Psikolog;
use App\Models\Pembayaran;
use Illuminate\Http\Request;
use App\Models\JadwalKonseling;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UpdatePasienRequest;
use Mockery\Generator\StringManipulation\Pass\Pass;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('pasien.dashboard',['title'=> "Dashboard"]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function cari_psikolog(Request $request)
    {
        $searchTerm    = $request->searchTerm;
        $get           = Psikolog::where('nama', 'like', '%' . $searchTerm . '%')->where('status','=','Diterima')->get();
        return view('pasien.psikolog',['title'=>"Cari Psikolog",'psikolog'=>$get,'searchTerm'=>$searchTerm]);
        Session::flash('error', 'Data Psikolog tidak ditemukan');
    }

    public function detail_psikolog($id)
    {
        $get = Psikolog::where('id',$id)->get();
        return view('pasien.detail_psikolog',['title'=>"Detail Psikolog",'psikolog'=>$get]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function buat_jadwal($id)
    {
        $get = Psikolog::where('id',$id)->get();
        return view('pasien.buat_jadwal',['title'=>"Buat Jadwal",'psikolog'=>$get]);
    }

    public function tambah(Request $request)
    {
        Validator::make($request->all(), [
            'id_pasien' => 'required',
            'id_psikolog' => 'required',
            'tanggal_konseling' => 'required',
            'jam_konseling' => 'required',
        ])->validate();
        $randomNumber = rand(10000000, 99999999);
        $request['id'] = $randomNumber;
        $data = $request->all();
        $jadwal = JadwalKonseling::create($data);
        Session::flash('success','Berhasil membuat jadwal konsultasi');
        if ($jadwal) {
            $id_jadwal = $request->id;
            $qr = '1231231';
            $data = array(
                'id_jadwal_konseling' => $id_jadwal,
                'qr' => $qr,
            );
            $pembayaran = Pembayaran::create($data);
            $balik = $pembayaran->id_jadwal_konseling;
        }
        return redirect('/pasien/pembayaran/'.$balik);
    }

    public function pembayaran($id)
    {
        $get = Pembayaran::where('id_jadwal_konseling',$id)->get();
        return view('pasien.pembayaran',['title'=> "Pembayaran",'pembayaran'=>$get]);
    }

    public function profil($id)
    {
        $get = Pasien::where('id',$id)->get();
        return view('pasien.profil',['title'=>"Profil",'profil'=>$get]);
    }

    public function detail_profil($id)
    {
        $get = Pasien::where('id',$id)->get();
        return view('pasien.detail_profil',['title'=>"Detail Profil",'profil'=>$get]);
    }

    public function edit_profil($id)
    {
        $get = Pasien::where('id',$id)->get();
        return view('pasien.edit_profil',['title'=>"Edit Profil",'profil'=>$get]);
    }

    public function update_profil($id,Request $request)
    {
        $data = Pasien::find($id);
        Validator::make($request->all(), [
            'nama' => 'required',
        ])->validate();
        $data->nama = $request->nama;
        $data->save();
        Session::flash('success',"mengubah profil");
        return redirect('/pasien/detail_profil/'.$id);
    }

}
