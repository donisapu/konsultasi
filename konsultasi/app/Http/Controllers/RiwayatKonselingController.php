<?php

namespace App\Http\Controllers;

use App\Models\RiwayatKonseling;
use App\Http\Requests\StoreRiwayatKonselingRequest;
use App\Http\Requests\UpdateRiwayatKonselingRequest;

class RiwayatKonselingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRiwayatKonselingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(RiwayatKonseling $riwayatKonseling)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(RiwayatKonseling $riwayatKonseling)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRiwayatKonselingRequest $request, RiwayatKonseling $riwayatKonseling)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(RiwayatKonseling $riwayatKonseling)
    {
        //
    }
}
