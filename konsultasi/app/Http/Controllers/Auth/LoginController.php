<?php

namespace App\Http\Controllers\Auth;

use App\Models\Pasien;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Psikolog;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function login_pasien()
    {
        return view('auth.login_pasien',['title'=> "Login Pasien"]);
    }

    public function pasien(Request $request)
    {
        $rules = [
            'username' => 'required',
            'password' => 'required|string'
        ];
        $messages = [
            'username.required' => 'username wajib diisi',
            'password.required' => 'Password wajib diisi',
            'password.string' => 'Password harus berupa string'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $pasien = Pasien::where([['username', $request->username]])->first();
        if($pasien){
            if (Hash::check($request->password, $pasien->password)) {
                //buat session
                $request->session()->put('id', $pasien->id);
                $request->session()->put('nama', $pasien->nama);
                $request->session()->put('username', $pasien->username);
                $request->session()->put('role', 'pasien');
                $request->session()->regenerate();

                return redirect()->route('pasien.dashboard');
            } else {
                Session::flash('error', 'Username atau password salah');
                return redirect('/login_pasien');
            }
        } else {
            Session::flash('error', 'Username atau password salah');
            return redirect('/login_pasien');
        }
    }

    public function pasien_logout(Request $request)
    {
        $request->session()->flush();
        Session::flash('error', 'Anda telah logout');
        return redirect('/login_pasien');
    }

    public function hapus_pasien($id)
    {
        $pasien = Pasien::find($id);
        $pasien->delete();
        return redirect('/login_pasien');
    }

    public function login_psikolog()
    {
        return view('auth.login_psikolog',['title'=> "Login Psikolog"]);
    }

    public function psikolog(Request $request)
    {
        $rules = [
            'username' => 'required',
            'password' => 'required|string'
        ];
        $messages = [
            'username.required' => 'username wajib diisi',
            'password.required' => 'Password wajib diisi',
            'password.string' => 'Password harus berupa string'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $psikolog = Psikolog::where([['username', $request->username]])->first();
        if($psikolog){
            if (Hash::check($request->password, $psikolog->password)) {
                //buat session
                $request->session()->put('id', $psikolog->id);
                $request->session()->put('nama', $psikolog->nama);
                $request->session()->put('username', $psikolog->username);
                $request->session()->put('email', $psikolog->email);
                $request->session()->put('nomor_telepon', $psikolog->nomor_telepon);
                $request->session()->put('tahun_lulus', $psikolog->tahun_lulus);
                $request->session()->put('asal_universitas', $psikolog->asal_universitas);
                $request->session()->put('tempat_praktik', $psikolog->tempat_praktik);
                $request->session()->put('nomor_str', $psikolog->nomor_str);
                $request->session()->put('role', 'psikolog');
                $request->session()->regenerate();
                if ($psikolog->status == "Diterima") {
                    return redirect()->route('psikolog.dashboard');
                }else{
                    Session::flash('error', 'Silahkan menunggu verifikasi admin');
                    return redirect('/login_psikolog');
                }

            } else {
                Session::flash('error', 'Username atau password salah');
                return redirect('/login_psikolog');
            }
        } else {
            Session::flash('error', 'Username atau password salah');
            return redirect('/login_psikolog');
        }
    }

    public function psikolog_logout(Request $request)
    {
        $request->session()->flush();
        Session::flash('error', 'Anda telah logout');
        return redirect('/login_psikolog');
    }

    public function hapus_psikolog($id)
    {
        $psikolog = Psikolog::find($id);
        $psikolog->delete();
        return redirect('/login_psikolog');
    }

    public function login_admin()
    {
        return view('auth.login_admin',['title'=> "Login Admin"]);
    }

    public function admin(Request $request)
    {
        $rules = [
            'username' => 'required',
            'password' => 'required|string'
        ];
        $messages = [
            'username.required' => 'username wajib diisi',
            'password.required' => 'Password wajib diisi',
            'password.string' => 'Password harus berupa string'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $admin = Admin::where([['username', $request->username]])->first();
        if($admin){
            if (Hash::check($request->password, $admin->password)) {
                //buat session
                $request->session()->put('id', $admin->id);
                $request->session()->put('nama', $admin->nama);
                $request->session()->put('username', $admin->username);
                $request->session()->put('role', 'admin');
                $request->session()->regenerate();

                return redirect()->route('admin.dashboard');
            } else {
                Session::flash('error', 'Username atau password salah');
                return redirect('/login_admin');
            }
        } else {
            Session::flash('error', 'Username atau password salah');
            return redirect('/login_admin');
        }
    }

    public function admin_logout(Request $request)
    {
        $request->session()->flush();
        Session::flash('error', 'Anda telah logout');
        return redirect('/login_admin');
    }

}
