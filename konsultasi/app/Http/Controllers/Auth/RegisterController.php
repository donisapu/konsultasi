<?php

namespace App\Http\Controllers\Auth;

use App\Models\Admin;
use App\Models\Pasien;
use App\Models\Psikolog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function daftar_pasien()
    {
        return view('auth.daftar_pasien',['title' => "Daftar Pasien"]);
    }

    public function register_pasien(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'username' => 'required|unique:pasiens,username',
            'password' => 'required',
        ],[
            'nama.required' => 'Nama harus diisi',
            'username.required' => 'Username harus diisi',
            'password.required' => 'Password harus diisi',
            'username.unique' => 'Username sudah digunakan'
        ]);
        $request['password'] = Hash::make($request['password']);
        $data = $request->all();
        Pasien::create($data);
        Session::flash('success',"Mendaftar Pasien");
        return redirect('login_pasien');
    }

    public function daftar_psikolog()
    {
        return view('auth.daftar_psikolog',['title' => "Daftar Psikolog"]);
    }

    public function register_psikolog(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'username' => 'required|unique:psikologs,username',
            'email' => 'required|unique:psikologs,email',
            'password' => 'required',
            'nomor_telepon' => 'required',
            'tahun_lulus' => 'required',
            'asal_universitas' => 'required',
            'tempat_praktik' => 'required',
            'nomor_str' => 'required',
            'file_str' => 'required',
            'foto' => 'required',
        ],[
            'nama.required' => 'Nama harus diisi',
            'username.required' => 'Username harus diisi',
            'password.required' => 'Password harus diisi',
            'username.unique' => 'Username sudah digunakan',
            'email.unique' => 'Email sudah digunakan',
            'email.required' => 'Email harus diisi',
            'nomor_telepon.required' => 'Nomor Telepon harus diisi',
            'tahun_lulus.required' => 'Tahun Lulus harus diisi',
            'asal_universitas.required' => 'Asal Universitas harus diisi',
            'tempat_praktik.required' => 'Tempat Praktik harus diisi',
            'nomor_str.required' => 'Nomor STR harus diisi',
            'file_str.required' => 'File STR harus disertakan',
            'foto.required' => 'Foto harus dipilih',
        ]);
        $request['password'] = Hash::make($request['password']);
        $data = $request->all();
        if ($request->hasFile('file_str')) {
            $str = $request->file('file_str');
            $directory = public_path('/psikolog/str');
            $namefile = time() . $str->getClientOriginalName();
            $str->move($directory, $namefile);
            $data['file_str'] = $namefile;
        }
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $directory = public_path('/psikolog/foto');
            $namefile = time() . $foto->getClientOriginalName();
            $foto->move($directory, $namefile);
            $data['foto'] = $namefile;
        }
        Psikolog::create($data);
        Session::flash('success',"Silahkan tunggu verifikasi");
        return redirect('login_psikolog');
    }

    public function daftar_admin()
    {
        return view('auth.daftar_admin',['title' => "Daftar Pasien"]);
    }

    public function register_admin(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'username' => 'required|unique:admins,username',
            'password' => 'required',
        ]);
        $request['password'] = Hash::make($request['password']);
        $data = $request->all();
        Admin::create($data);
        Alert::success('Berhasil', 'Berhasil daftar akun');
        return redirect('login_admin');
    }

}
