<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jadwal_konselings', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->primary();
            $table->integer('id_pasien');
            $table->integer('id_psikolog');
            $table->date('tanggal_konseling');
            $table->time('jam_konseling');
            $table->enum('status_konselsing',['Belum Bayar','Telah Terjadwalkan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jadwal_konselings');
    }
};
