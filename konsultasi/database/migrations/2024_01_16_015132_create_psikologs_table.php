<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('psikologs', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('username');
            $table->string('email');
            $table->string('password');
            $table->string('nomor_telepon');
            $table->string('tahun_lulus');
            $table->string('asal_universitas');
            $table->string('tempat_praktik');
            $table->string('nomor_str');
            $table->string('file_str');
            $table->string('foto');
            $table->float('harga')->nullable();
            $table->enum('status', ['Menunggu', 'Diterima', 'Ditolak']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('psikologs');
    }
};
