<?php

use App\Models\Chat;
use App\Models\Psikolog;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PasienController;
use App\Http\Controllers\PsikologController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\JadwalKonselingController;
use Mockery\Generator\StringManipulation\Pass\Pass;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

//ADMIN
Route::get('/daftar_admin', [RegisterController::class, 'daftar_admin'])->name('daftar_admin');
Route::post('/register_admin', [RegisterController::class, 'register_admin'])->name('register_admin');
Route::get('/login_admin', [LoginController::class, 'login_admin'])->name('login_admin');
Route::post('/login/admin',[LoginController::class,'admin']);
Route::get('/logout/admin', [LoginController::class, 'admin_logout']);
Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => 'admin'], function () {
    Route::get('/dashboard',[AdminController::class,'index'])->name('dashboard');
    Route::get('/list_psikolog',[AdminController::class,'list_psikolog']);
    Route::get('/verifikasi/{id}',[AdminController::class,'verifikasi']);
    Route::get('/terima/{id}',[AdminController::class,'terima']);
    Route::get('/tolak/{id}',[AdminController::class,'tolak']);
    Route::get('/user',[AdminController::class,'daftar_user']);
    Route::get('/profil_pasien/{id}',[AdminController::class,'profil_pasien']);
    Route::get('/profil_psikolog/{id}',[AdminController::class,'profil_psikolog']);
    Route::get('/edit_psikolog/{id}',[AdminController::class,'edit_psikolog']);
    Route::post('/update_psikolog/{id}',[AdminController::class,'update_psikolog']);
    Route::get('/hapus_psikolog/{id}',[AdminController::class,'hapus_psikolog']);
    Route::get('/edit_pasien/{id}',[AdminController::class,'edit_pasien']);
    Route::post('/update_pasien/{id}',[AdminController::class,'update_pasien']);
    Route::get('/hapus_pasien/{id}',[AdminController::class,'hapus_pasien']);
    Route::get('riwayat_konseling',[AdminController::class,'riwayat_konseling']);

});

//PSIKOLOG
Route::get('/daftar_psikolog', [RegisterController::class, 'daftar_psikolog'])->name('daftar_psikolog');
Route::post('/register_psikolog', [RegisterController::class, 'register_psikolog'])->name('register_psikolog');
Route::get('/login_psikolog', [LoginController::class, 'login_psikolog'])->name('login_psikolog');
Route::post('/login/psikolog',[LoginController::class,'psikolog']);
Route::get('/logout/psikolog', [LoginController::class, 'psikolog_logout']);
Route::post('/hapus/psikolog/{id}', [LoginController::class, 'hapus_psikolog']);
Route::group(['prefix' => 'psikolog', 'as' => 'psikolog.','middleware' => 'psikolog'], function () {
    Route::get('/dashboard',[PsikologController::class,'index'])->name('dashboard');
    Route::get('/konseling',[JadwalKonselingController::class,'index_psikolog']);
    Route::get('/profil',[PsikologController::class,'profil']);
    Route::get('/detail_profil/{id}',[PsikologController::class,'detail_profil']);
    Route::get('/edit_profil/{id}',[PsikologController::class,'edit_profil']);
    Route::post('/update/{id}',[PsikologController::class,'update_profil']);
});

//PASIEN
Route::get('/daftar_pasien', [RegisterController::class, 'daftar_pasien'])->name('daftar_pasien');
Route::post('/register_pasien', [RegisterController::class, 'register_pasien'])->name('register_pasien');
Route::get('/login_pasien', [LoginController::class, 'login_pasien'])->name('login_pasien');
Route::post('/login/pasien', [LoginController::class, 'pasien'])->name('pasien');
Route::get('/logout/pasien', [LoginController::class, 'pasien_logout']);
Route::get('/hapus/pasien', [LoginController::class, 'hapus_pasien']);
Route::group(['prefix' => 'pasien', 'as' => 'pasien.','middleware' => 'pasien'], function () {
    Route::get('/dashboard', [PasienController::class, 'index'])->name('dashboard');
    Route::post('/cari_psikolog',[PasienController::class,'cari_psikolog']);
    Route::get('/detail_psikolog/{id}',[PasienController::class,'detail_psikolog']);
    Route::get('/buat_jadwal/{id}',[PasienController::class,'buat_jadwal']);
    Route::post('/tambah_jadwal',[PasienController::class,'tambah'])->name('tambah_jadwal');
    Route::get('/pembayaran/{id}',[PasienController::class,'pembayaran']);
    Route::get('/profil/{id}',[PasienController::class,'profil']);
    Route::get('/detail_profil/{id}',[PasienController::class,'detail_profil']);
    Route::get('/edit_profil/{id}',[PasienController::class,'edit_profil']);
    Route::post('/update/{id}',[PasienController::class,'update_profil']);
    Route::get('/konseling',[JadwalKonselingController::class,'index_pasien']);
    Route::get('/detail_jadwal/{id}',[JadwalKonselingController::class,'detail_jadwal']);
    Route::get('/chat/{id}',[ChatController::class,'pasien']);
});
