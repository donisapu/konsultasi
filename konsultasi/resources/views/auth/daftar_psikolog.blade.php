@extends('layout')
@section('container')

<div class="container-fluid ps-md-0">
    <div class="row g-0">
      <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
      <div class="col-md-8 col-lg-6">
        <div class="login d-flex align-items-center py-5">
          <div class="container">
            <div class="row">
              <div class="col-md-9 col-lg-8 mx-auto">
                <h3 class="login-heading mb-4">Daftar Psikolog</h3>
                @if ($errors->any())
                                    <div>
                                        <div class="alert alert-danger" role="alert">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                <!-- Sign In Form -->
                <form method="POST" action="{{ 'register_psikolog' }}" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                  <div class="form-floating mb-3">
                    <input type="text" name="nama" class="form-control" id="floatingInput" placeholder="name@example.com">
                    <label for="floatingInput">Nama Psikolog</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="text" name="username" class="form-control" id="floatingInput" placeholder="name@example.com">
                    <label for="floatingInput">Username</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="text" name="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                    <label for="floatingInput">Email</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
                    <label for="floatingPassword">Password</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="text" name="nomor_telepon" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                    <label for="floatingInput">Nomor Telepon (harus terdaftar pada aplikasi DANA)</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="text" name="tahun_lulus" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                    <label for="floatingInput">Tahun Lulus</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="text" name="asal_universitas" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                    <label for="floatingInput">Asal Universitas</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="text" name="tempat_praktik" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                    <label for="floatingInput">Tempat Praktik</label>
                  </div>
                  <div class="row">
                  <div class="col-md-5">
                  <div class="form-floating mb-3">
                      <input type="text" name="nomor_str" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                      <label for="floatingInput">Nomor STR</label>
                  </div>
                </div>
                <div class="col-md-7">
                    <div class="mb-3">
                        <label for="floatingInput">File STR</label>
                        <input type="file" name="file_str" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                   </div>
                 </div>
            </div>

                 <div class="mb-3">
                      <label for="floatingInput">Foto</label>
                    <input type="file" name="foto" class="form-control" id="floatingInput" placeholder="Nomor telepon harus terdaftar pada aplikasi DANA">
                  </div>

                  <div class="d-grid">
                    <button class="btn btn-lg btn-primary btn-login text-uppercase fw-bold mb-2" type="submit">Sign Up</button>
                    <div class="text-center">
                    <a href="{{ 'login_psikolog' }}" class="small">Halaman login Psikolog</a>
                    </div>
                </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



@endsection
