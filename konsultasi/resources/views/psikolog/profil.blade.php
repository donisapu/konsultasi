@extends('psikolog.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">


            @foreach ($profil as $key)
                <a href="{{ '/psikolog/detail_profil/' . $key->id }}" class="btn btn-block btn-primary btn-lg">
                    <h5>Lihat Profil</h5>
                </a><br>
                <form action="{{ '/logout/psikolog' }}" method="post">
                    @csrf
                    <a href="{{ '/logout/psikolog' }}" type="submit" class="btn btn-block btn-success btn-lg"><h5>Logout</h5></a><br>
                </form>
                <form action="{{ '/hapus/psikolog/' . $key->id }}" method="post">
                    @csrf
                    <button class="btn btn-block btn-danger btn-lg" type="submit">
                        <h5>Hapus Akun</h5>
                    </button>
                </form>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
