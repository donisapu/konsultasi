@extends('psikolog.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">

                <!-- Row start -->
                <div class="row gutters">

                    <div class="col-lg-2 col-sm-4 col-12">
                        <div class="hospital-tiles">
                            <img src="img/hospital/appointment.svg" alt="Quality Dashboards" />
                            <p>Jadwal Konseling Mendatang</p>
                            <h2>{{ $jadwal }}</h2>
                        </div>
                    </div>

                </div>
                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
