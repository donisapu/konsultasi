<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap Dashboards">
    <meta name="author" content="Bootstrap Gallery">
    <link rel="shortcut icon" href="{{ asset('dash/img/favicon.svg') }}" />

    <!-- Title -->
    <title>{{ $title }}</title>


    <!-- *************
   ************ Common Css Files *************
   ************ -->
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="{{ asset('dash/css/bootstrap.min.css') }}">

    <!-- Icomoon Font Icons css -->
    <link rel="stylesheet" href="{{ asset('dash/fonts/style.css') }}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('dash/css/main.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11">
    <link rel="stylesheet" href="{{ asset('dash/vendor/datepicker/css/classic.css') }}" />
	<link rel="stylesheet" href="{{ asset('dash/vendor/datepicker/css/classic.date.css') }}" />


    <!-- *************
   ************ Vendor Css Files *************
  ************ -->
<style>
    @media (max-width: 767px) {
  .abc {
    max-width: 100%;
  }
}
</style>
</head>

<body>

    <!-- Loading starts -->
    {{-- <div id="loading-wrapper">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div> --}}
    <!-- Loading ends -->

    <!-- Header start -->
    <header class="header">
        <div class="container-fluid">

            <!-- Row start -->
            <div class="row gutters">
                <div class="col-sm-4 col-4">
                    <a href="index.html" class="logo">Konsultasi Kita</span></a>
                </div>
                <div class="col-sm-8 col-8">

                    <!-- Header actions start -->
                    <ul class="header-actions">
                        <li class="dropdown">
                            <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                                aria-haspopup="true">
                                <span class="user-name">{{ session()->get('nama') }}</span>
                                <span class="avatar">PSI<span class="status busy"></span></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userSettings">
                                <div class="header-profile-actions">

                                    <form action="{{ '/logout/psikolog' }}" method="post">
                                        @csrf
                                        <a href="{{ '/logout/psikolog' }}" type="submit"><i class="icon-log-out1"></i> Sign Out</a>
                                    </form>
                                </div>
                            </div>
                            {{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userSettings">
                                <div class="header-profile-actions">
                                    <div class="header-user-profile">
                                        <div class="header-user">
                                            <img src="{{ asset('dash/img/user11.png') }}" alt="Medical Dashboards" />
                                        </div>
                                        <h5>{{ session()->get('nama') }}</h5>
                                        <p>Admin</p>
                                    </div>
                                    <a href="hospital-add-doctor.html"><i class="icon-user1"></i> My Profile</a>
                                    <a href="account-settings.html"><i class="icon-settings1"></i> Account Settings</a>
                                    <a href="hospital-reviews.html"><i class="icon-activity"></i> Activity Logs</a>
                                    <a href="login.html"><i class="icon-log-out1"></i> Sign Out</a>
                                </div>
                            </div> --}}
                        </li>
                    </ul>
                    <!-- Header actions end -->

                </div>
            </div>
            <!-- Row end -->

        </div>
    </header>
    <div class="container-fluid">


        <!-- Navigation start -->
        <nav class="navbar navbar-expand-lg custom-navbar">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#royalHospitalsNavbar"
                aria-controls="royalHospitalsNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="royalHospitalsNavbar">
                <ul class="navbar-nav" style="align-content: center">
                    <li class="nav-item">
                        <a class="nav-link active-page" href="{{ '/psikolog/dashboard' }}">
                            <i class="icon-devices_other nav-icon"></i>
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active-page" href="{{ '/psikolog/konseling/' }}">
                            <i class="icon-calendar nav-icon"></i>
                            Konseling yang akan Datang
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active-page" href="{{ '/psikolog/konseling/' }}">
                            <i class="icon-clock nav-icon"></i>
                            Riwayat Konseling
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active-page" href="{{ '/psikolog/profil/'}}">
                            <i class="icon-user nav-icon"></i>
                            Profil
                        </a>
                    </li>

                </ul>
            </div>
        </nav>

        @include('sweetalert::alert')
        @yield('container')

        <footer class="main-footer">© </footer>

    </div>

    <!-- *************
   ************ Required JavaScript Files *************
  ************* -->
    <!-- Required jQuery first, then Bootstrap Bundle JS -->
    <script src="{{ asset('dash/js/jquery.min.js') }}"></script>
    <script src="{{ asset('dash/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dash/js/moment.js') }}"></script>


    <!-- *************
   ************ Vendor Js Files *************
  ************* -->

    <!-- Apex Charts -->
    <script src="{{ asset('dash/vendor/apex/apexcharts.min.js') }}"></script>
    <script src="{{ asset('dash/vendor/apex/examples/mixed/hospital-line-column-graph.js') }}"></script>
    <script src="{{ asset('dash/vendor/apex/examples/mixed/hospital-line-area-graph.js') }}"></script>
    <script src="dash/vendor/apex/examples/bar/hospital-patients-by-age.js"></script>

    <!-- Rating JS -->
    <script src="{{ asset('dash/vendor/rating/raty.js') }}"></script>
    <script src="{{ asset('dash/vendor/rating/raty-custom.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Datepickers -->
    <script src="{{ asset('dash/vendor/datepicker/js/picker.js') }}"></script>
    <script src="{{ asset('dash/vendor/datepicker/js/picker.date.js') }}"></script>
    <script src="{{ asset('dash/vendor/datepicker/js/custom-picker.js') }}"></script>

    <!-- Main Js Required -->
    <script src="{{ asset('dash/js/main.js') }}"></script>
    <script>
        // Initialization for ES Users
        import {
            Ripple,
            initMDB
        } from "mdb-ui-kit";

        initMDB({
            Ripple
        });
    </script>
    <script>
        // Check if there's a success message from the server
        @if(session('Berhasil'))
            // Display SweetAlert with success message
            Swal.fire({
                icon: 'success',
                title: 'Berhasil',
                text: '{{ session('success') }}',
            });
        @endif
    </script>

</body>

</html>
