@extends('psikolog.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
            @foreach ($profil as $key)
            <div class="row gutters">
                <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                    <figure class="user-card">
                        <input type="text" class="form-control" readonly value="{{ $key->nama }}">
                        <input type="text" class="form-control mt-4" readonly value="{{ $key->username }}">
                        <input type="text" class="form-control mt-4" readonly value="{{ $key->email }}">
                        <input type="text" class="form-control mt-4" readonly value="{{ $key->nomor_telepon }}">
                    </figure>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                    <figure class="user-card">
                        <input type="text" class="form-control" readonly value="{{ $key->tahun_lulus }}">
                        <input type="text" class="form-control mt-4" readonly value="{{ $key->asal_universitas }}">
                        <input type="text" class="form-control mt-4" readonly value="{{ $key->tempat_praktik }}">
                        <input type="text" class="form-control mt-4" readonly value="{{ $key->nomor_str }}">
                    </figure>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                    <figure class="user-card">
                        <figcaption>
                            <img src="{{ asset('psikolog/foto/'.$key->foto) }}" alt="Medical Dashboards" style="width: 100%">

                        </figcaption>
                    </figure>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ '/psikolog/edit_profil/'.$key->id }}" class="btn btn-primary btn-block">Edit Profil</a>
                </div>
            </div>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
