@extends('psikolog.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @foreach ($jadwal as $key)
            @php
                $now = \Carbon\Carbon::now('Asia/Jakarta');
                if ($now >= $key->jam_konseling && $key->status_konseling != "Belum Bayar") {
                    $link= '/psikolog/chat/'.$key->id;
                }else {
                    $link= 'javascript:void(0)';
                }
            @endphp
            <div class="col-lg-12 col-md-12 col-12">
                <a href="{{ $link }}" class="hospital-list">
                    {{-- <img src="{{ asset('psikolog/foto/'.$key->foto) }}" class="hospital-thumb" alt="Medical Dashboards" /> --}}
                    <div class="hospital-details">
                        <div class="hospital-location">
                            <h5>Anda memiliki jadwal konseling pada {{ \Carbon\Carbon::parse($key->tanggal_konseling)->format('d F Y') }}
                                pukul {{ $key->jam_konseling }}</h5>
                            {{-- <p>Anda memiliki jadwal konseling pada {{ \Carbon\Carbon::parse($key->tanggal_konseling)->format('d F Y') }}
                                pukul {{ $key->jam_konseling }}</p> --}}
                                @if ($key->status_konseling == 'Belum Bayar')
                                <span class="badge badge-warning float-right">{{ $key->status_konseling }}</span>
                                @else
                                <span class="badge badge-primary float-right">{{ $key->status_konseling }}</span>
                                @endif

                        </div>

                    </div>
                </a>
            </div>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
