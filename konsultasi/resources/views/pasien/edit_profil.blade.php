@extends('pasien.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @foreach ($profil as $key)
            <div class="row gutters">
                <div class="col-xl-3 col-md-4 col-sm-6 col-12">
                    <figure class="user-card">
                        <figcaption>
                            <form action="{{ '/pasien/update/'.$key->id }}" method="post">
                                @csrf
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{ $key->nama }}"><br>

                                <button class="btn btn-primary btn-block" type="submit">Update</button>
                            </form>
                        </figcaption>
                    </figure>
                </div>
            </div>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
