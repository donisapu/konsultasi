@extends('pasien.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @foreach ($psikolog as $psy)
            <h5>Buat jadwal dengan <b>{{ $psy->nama }}</b></h5>
            <form action="{{ '/pasien/tambah_jadwal' }}" method="post">
                @csrf
                @php
                    $randomNumber = rand(10000000, 99999999);
                @endphp
                <input type="hidden" name="id_pasien" value="{{ session()->get('id') }}" id="">
                <input type="hidden" name="id_psikolog" value="{{ $psy->id }}" id="">
                <input type="date" required style="margin-top: 100px" name="tanggal_konseling" class="form-control" placeholder="Pilih Tanggal" min="{{ \Carbon\Carbon::now()->format('Y-m-d') }}">

                <input type="time" required name="jam_konseling" class="form-control mt-4" placeholder="Pilih Waktu" id="">
                <button type="submit" style="border-radius:5px;margin-top:150px" class="btn btn-primary btn-block">Buat Jadwal</button>
            </form>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
