@extends('pasien.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">

                <div class="row gutters">
                    <div class="col-sm-12">
                        <div class="card">
                            @if ($psikolog->count() > 0)
                                <div class="card-header">
                                    <div class="card-title">Data Psikolog</div>
                                </div>
                                <div class="card-body">
                                    @foreach($psikolog as $psy)
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img style="height:100%;width:100%" src="{{ asset('psikolog/foto/'.$psy->foto) }}" alt="">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>
                                                            <tr>
                                                                <td><b>Nama</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->nama }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Nomor Telepon</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->nomor_telepon }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Harga</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->harga }}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Asal Universitas</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->asal_universitas }}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Tahun Lulus</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->tahun_lulus }}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Tempat Praktik</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->tempat_praktik }}</b></td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                    <a href="{{ '/pasien/buat_jadwal/'.$psy->id }}" class="btn btn-primary btn-block mt-4">Buat Jadwal dengan Psikolog</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                @endforeach
                                </div>
                            @else
                                <div class="card-header">
                                    <div class="card-title">Psikolog Tidak Ditemukan</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
