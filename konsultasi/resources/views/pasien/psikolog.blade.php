@extends('pasien.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">

                <!-- Row start -->
                <div class="row gutters">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Cari Psikolog</div>
                            </div>
                            <div class="card-body">

                                <form action="{{ 'cari_psikolog' }}" method="post">
                                    <div class="input-group">
                                        @csrf
                                        <input type="search" name="searchTerm" class="form-control rounded"
                                            placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                        <button type="submit" class="btn btn-outline-primary"
                                            data-mdb-ripple-init><span class="icon-search1"></span></button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row gutters">
                    <div class="col-sm-12">
                        <div class="card">
                            @if ($searchTerm && $psikolog->count() > 0)
                                <div class="card-header">
                                    <div class="card-title">Data Psikolog</div>
                                </div>
                                <div class="card-body">
                                    @foreach($psikolog as $psy)
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <img style="height:100%;width:100%" src="{{ asset('psikolog/foto/'.$psy->foto) }}" alt="">
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="table-responsive">
                                                    <table class="table m-0">
                                                        <tbody>
                                                            <tr>
                                                                <td><b>Nama</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->nama }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Harga</b></td>
                                                                <td>:</td>
                                                                <td><b>{{ $psy->harga }}</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3"><a href="{{ 'detail_psikolog/'.$psy->id }}" class="btn btn-primary">Detail Psikolog</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                @endforeach
                                </div>
                            @else
                                <div class="card-header">
                                    <div class="card-title">Psikolog Tidak Ditemukan</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
