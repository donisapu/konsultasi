@extends('pasien.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @foreach ($profil as $key)
            <div class="row gutters">
                <div class="col-xl-3 col-md-4 col-sm-6 col-12">
                    <figure class="user-card">
                        <figcaption>
                            <img src="{{ asset('pasien/patient.png') }}" alt="Medical Dashboards" class="profile">
                            <h5>{{ $key->nama }}</h5>
                            <h6 class="designation">{{ $key->username }}</h6>
                            <div class="clearfix">
                                <a href="{{ '/pasien/edit_profil/'.$key->id }}" class="btn btn-info">
                                    Edit Profil
                                </a>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
