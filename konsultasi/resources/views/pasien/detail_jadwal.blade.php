@extends('pasien.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @foreach ($jadwal as $key)
                <div class="row gutters">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Detail Jadwal Konseling</div>
                            </div>
                            <div class="card-body">
                                <center><img src="{{ asset('psikolog/foto/' . $key->foto) }}" width="50%" alt="">
                                </center>
                                <div class="table-responsive mt-2">
                                    <table class="table m-0">
                                        <tr>
                                            <th>Nama Psikolog</th>
                                            <th> : </th>
                                            <th>{{ $key->nama }}</th>
                                        </tr>
                                        <tr>
                                            <th>Nama Pasien</th>
                                            <th> : </th>
                                            <th>{{ session()->get('nama') }}</th>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Konseling</th>
                                            <th> : </th>
                                            <th>{{ $key->tanggal_konseling }}</th>
                                        </tr>
                                        <tr>
                                            <th>Durasi Konseling</th>
                                            <th> : </th>
                                            <th>1 Jam</th>
                                        </tr>
                                        <tr>
                                            <th>Waktu Konseling</th>
                                            <th> : </th>
                                            <th>{{ \Carbon\Carbon::parse($key->jam_konseling)->format('H:i') }}</th>
                                        </tr>
                                        <tr>
                                            <th>Harga</th>
                                            <th> : </th>
                                            <th>{{ $key->harga }}</th>
                                        </tr>
                                        <tr>
                                            <th><a href="{{ '/pasien/chat/'.$id_jadwal }}" class="btn btn-primary">Chat</a></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->
    </div>
@endsection
