@extends('pasien.layout')
@section('container')
    <div class="main-container">

        <div class="content-wrapper">
            @foreach ($profil as $key)
                <a href="{{ '/pasien/detail_profil/' . $key->id }}" class="btn btn-block btn-primary btn-lg"><h5>Lihat Profil</h5></a><br>
                <form action="{{ '/logout/pasien' }}" method="post">
                    @csrf
                    <a href="{{ '/logout/pasien'  }}" type="submit" class="btn btn-block btn-success btn-lg"><h5>Logout</h5></a><br>
                </form>
                <a href="{{ '/hapus/pasien/' . $key->id }}" class="btn btn-block btn-danger btn-lg hapus-data" ><h5>Hapus Akun</h5></a>
            @endforeach

            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
