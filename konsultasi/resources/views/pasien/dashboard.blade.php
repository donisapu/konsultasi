@extends('pasien.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">

                <!-- Row start -->
                <div class="row gutters">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Cari Psikolog</div>
                            </div>
                            <div class="card-body">

                                <form action="{{ 'cari_psikolog' }}" method="post">
                                    <div class="input-group">
                                        @csrf
                                        <input type="search" name="searchTerm" class="form-control rounded"
                                            placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                                        <button type="submit" class="btn btn-outline-primary"
                                            data-mdb-ripple-init> <span class="icon-search1"></span> </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
