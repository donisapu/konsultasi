@extends('admin.layout')
@section('container')
    <div class="main-container">
        <div class="content-wrapper">

            <!-- Row start -->
            <div class="row gutters">
                @foreach ($pasien as $key)
                    <div class="col-lg-8 col-md-12 col-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <tr>
                                        <td>
                                            <h5>Nama</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->nama }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Username</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->username }}</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-6 col-6">
                    <a href="{{ '/admin/edit_pasien/'.$key->id }}" class="btn btn-primary btn-lg">Edit Profil</a>
                    <a href="{{ '/admin/hapus_pasien/'.$key->id }}" class="btn btn-danger btn-lg hapus-data">Hapus Pasien</a>
                </div>
            </div>
            @endforeach
            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
