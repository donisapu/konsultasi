@extends('admin.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">

                <!-- Row start -->
                <div class="row gutters">
                    <div class="col-sm-2"></div>
                    <div class="col-lg-4 col-sm-4 col-12  mt-5">
                        <a href="{{ '/admin/list_psikolog' }}">
                        <div class="hospital-tiles">
                            <img src="{{ asset('/verif.png') }}" alt="Quality Dashboards" />
                            <h5>Psikolog Menunggu Persetujuan</h5>
                        </div>
                    </a>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-12  mt-5">
                        <a href="{{ '/admin/user' }}">
                        <div class="hospital-tiles">
                            <img src="{{ asset('/user.png') }}" alt="Quality Dashboards" />
                            <h5>Daftar User</h5>
                        </div>
                    </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-lg-4 col-sm-4 col-12 mt-5">
                        <a href="{{ '/admin/riwayat_konseling' }}">
                        <div class="hospital-tiles">
                            <img src="{{ asset('/riwayat.png') }}" alt="Quality Dashboards" />

                            <h5>Daftar Riwayat Konseling</h5>
                        </div>
                    </a>
                    </div>
                </div>
                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
