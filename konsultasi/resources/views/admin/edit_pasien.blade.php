@extends('admin.layout')
@section('container')


        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
       <div class="main-container">


        <!-- Page header start -->

        <!-- Page header end -->

        <!-- Content wrapper start -->
        <div class="content-wrapper">

            <!-- Row start -->
            @foreach ($pasien as $key)
            <form action="{{ '/admin/update_pasien/'.$key->id }}" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="row gutters">
                <div class="col-lg-6 col-md-12 col-12">
                    <input type="text" class="form-control" name="nama" value="{{ $key->nama }}" id=""><br>
                    <input type="text" class="form-control" name="username" value="{{ $key->username }}" id=""><br>
                    <input type="text" class="form-control" name="password" placeholder="Ubah Password" id=""><br>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-primary btn-lg mt-4">Simpan</button>
                    <a href="{{ '/admin/profil_pasien/'.$key->id }}" class="btn btn-secondary btn-lg mt-4">Kembali</a>
                </div>
            </div>
            @endforeach
            </form>
            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
