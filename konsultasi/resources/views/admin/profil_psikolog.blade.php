@extends('admin.layout')
@section('container')
    <div class="main-container">
        <div class="content-wrapper">

            <!-- Row start -->
            <div class="row gutters">
                @foreach ($psikolog as $key)
                    <div class="col-lg-8 col-md-12 col-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table m-0">
                                    <tr>
                                        <td>
                                            <h5>Nama</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->nama }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Username</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->username }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Email</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->email }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Nomor Telepon</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->nomor_telepon }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Tahun Lulus</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->tahun_lulus }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Asal Universitas</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->asal_universitas }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Tempat Praktik</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->tempat_praktik }}</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5>Nomor STR</h5>
                                        </td>
                                        <td>
                                            <h5>:</h5>
                                        </td>
                                        <td>
                                            <h5>{{ $key->nomor_str }}</h5>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-3 col-md-4">
                        <a target="_blank" href="{{ asset('/psikolog/foto/' . $key->foto) }}">
                            <img src="{{ asset('/psikolog/foto/' . $key->foto) }}" style="width: 100%;" alt="">
                        </a>

                        <div class="row">
                            <a target="_blank" href="{{ asset('/psikolog/str/' . $key->file_str) }}">
                                <img src="{{ asset('/psikolog/str/' . $key->file_str) }}" style="width: 100%"
                                    alt="">
                            </a>

                        </div>
                    </div>

            </div>
            <div class="row mt-3">
                <div class="col-lg-6 col-6">
                    <a href="{{ '/admin/edit_psikolog/'.$key->id }}" class="btn btn-primary btn-lg">Edit Profil</a>
                    <a href="{{ '/admin/hapus_psikolog/'.$key->id }}" class="btn btn-danger btn-lg hapus-data">Hapus Psikolog</a>
                </div>
            </div>
            @endforeach
            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
