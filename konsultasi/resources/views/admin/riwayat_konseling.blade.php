
<!doctype html>
<html lang="en">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Meta -->
		<meta name="description" content="Responsive Bootstrap Dashboards">
		<meta name="author" content="Bootstrap Gallery">
		<link rel="shortcut icon" href="img/favicon.svg" />

		<!-- Title -->
		<title>Medical Admin Template - Data Tables</title>


		<!-- *************
			************ Common Css Files *************
			************ -->
		<!-- Bootstrap css -->
		<link rel="stylesheet" href="{{ asset('dash/css/bootstrap.min.css') }}">

		<!-- Icomoon Font Icons css -->
		<link rel="stylesheet" href="{{ asset('dash/fonts/style.css') }}">

		<!-- Main css -->
		<link rel="stylesheet" href="{{ asset('dash/css/main.min.css') }}">


		<!-- *************
			************ Vendor Css Files *************
		************ -->
		<!-- Data Tables -->
		<link rel="stylesheet" href="{{ asset('dash/vendor/datatables/dataTables.bs4.css') }}" />
		<link rel="stylesheet" href="{{ asset('dash/vendor/datatables/dataTables.bs4-custom.css') }}" />


	</head>

	<body>

		<!-- Header start -->
		<header class="header">
            <div class="container-fluid">

                <!-- Row start -->
                <div class="row gutters">
                    <div class="col-sm-4 col-4">
                        <a href="index.html" class="logo">Konsultasi Kita</span></a>
                    </div>
                    <div class="col-sm-8 col-8">

                        <!-- Header actions start -->
                        <ul class="header-actions">
                            <li class="dropdown">
                                <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                                    aria-haspopup="true">
                                    <span class="user-name">{{ session()->get('nama') }}</span>
                                    <span class="avatar">ADM<span class="status online"></span></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userSettings">
                                    <div class="header-profile-actions">

                                        <form action="{{ '/logout/admin' }}" method="post">
                                            @csrf
                                            <a href="{{ '/logout/admin' }}" type="submit"><i class="icon-log-out1"></i> Sign Out</a>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!-- Header actions end -->

                    </div>
                </div>
                <!-- Row end -->

            </div>
        </header>
		<!-- Header end -->

		<!-- *************
			************ Header section end *************
		************* -->


		<div class="container-fluid">


			<!-- Navigation start -->
			<nav class="navbar navbar-expand-lg custom-navbar">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#royalHospitalsNavbar"
                    aria-controls="royalHospitalsNavbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <i></i>
                        <i></i>
                        <i></i>
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="royalHospitalsNavbar">
                    <ul class="navbar-nav" style="align-content: center">
                        <li class="nav-item">
                            <a class="nav-link active-page" href="{{ '/admin/dashboard' }}">
                                <i class="icon-devices_other nav-icon"></i>
                                Dashboard
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
			<!-- Navigation end -->


			<!-- *************
				************ Main container start *************
			************* -->
			<div class="main-container">

				<!-- Content wrapper start -->
				<div class="content-wrapper">

					<!-- Row start -->
					<div class="row gutters">
						<div class="col-sm-12">

							<div class="table-container">
								<h5 class="table-title">Fixed Header</h5>
								<!--*************************
									*************************
									*************************
									Fixed Header table start
								*************************
								*************************
								*************************-->
								<div class="table-responsive">
									<table id="fixedHeader" class="table">
										<thead>
											<tr>
												<th>No</th>
												<th>Psikolog</th>
												<th>Nomor STR</th>
												<th>Pasien</th>
												<th>Tanggal</th>
												<th>Waktu</th>
											</tr>
										</thead>
										<tbody>
                                            @php
                                                $no=1;
                                            @endphp
                                            @foreach ($riwayat as $key)
											<tr>
												<td>{{ $no++ }}</td>
												<td>{{ $key->nama_psikolog }}</td>
												<td>{{ $key->nomor_str }}</td>
												<td>{{ $key->nama_pasien }}</td>
												<td>{{ $key->tanggal_konseling }}</td>
												<td>{{ $key->jam_konseling }}</td>
											</tr>
                                            @endforeach
										</tbody>
									</table>
								</div>
							</div>


						</div>

					</div>
					<!-- Row end -->

				</div>
				<!-- Content wrapper end -->


			</div>
			<!-- *************
				************ Main container end *************
			************* -->


		</div>

		<!-- *************
			************ Required JavaScript Files *************
		************* -->
		<!-- Required jQuery first, then Bootstrap Bundle JS -->
		<script src="{{ asset('dash/js/jquery.min.js') }}"></script>
		<script src="{{ asset('dash/js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('dash/js/moment.js') }}"></script>


		<!-- *************
			************ Vendor Js Files *************
		************* -->
		<!-- Data Tables -->
		<script src="{{ asset('dash/vendor/datatables/dataTables.min.js') }}"></script>
		<script src="{{ asset('dash/vendor/datatables/dataTables.bootstrap.min.js') }}"></script>

		<!-- Custom Data tables -->
		<script src="{{ asset('dash/vendor/datatables/custom/custom-datatables.js') }}"></script>
		<script src="{{ asset('dash/vendor/datatables/custom/fixedHeader.js') }}"></script>


		<!-- Main Js Required -->
		<script src="{{ asset('dash/js/main.js') }}"></script>

	</body>

</html>
