@extends('admin.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">
                <div class="card plain">
                    <div class="card-body">
                <!-- Row start -->
                <div class="row gutters">
                    <h3 class="mb-3">PASIEN</h3>
                    @foreach ($pasien as $key)
                    <div class="col-lg-12 col-md-12 col-12">
                        <a href="{{ '/admin/profil_pasien/'.$key->id }}" class="hospital-list">
                            <div class="hospital-details">
                                <div class="hospital-location">
                                    <h4>Nama : {{ $key->nama }}</h4>
                                    <h5>Username : {{ $key->username }}</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                <div class="row gutters">
                    <h3 class="mt-5 mb-3">PSIKOLOG</h3>
                    @foreach ($psikolog as $key)
                    <div class="col-lg-12 col-md-12 col-12">
                        <a href="{{ '/admin/profil_psikolog/'.$key->id }}" class="hospital-list">
                            <div class="hospital-details">
                                <div class="hospital-location">
                                    <h4>Nama : {{ $key->nama }}</h4>
                                    <h5>Username : {{ $key->username }}</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                    </div>
                </div>
                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
