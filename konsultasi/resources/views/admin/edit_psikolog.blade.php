@extends('admin.layout')
@section('container')


        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
       <div class="main-container">


        <!-- Page header start -->

        <!-- Page header end -->

        <!-- Content wrapper start -->
        <div class="content-wrapper">

            <!-- Row start -->
            @foreach ($psikolog as $key)
            <form action="{{ '/admin/update_psikolog/'.$key->id }}" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="row gutters">
                <div class="col-lg-6 col-md-12 col-12">
                    <input type="text" class="form-control" name="nama" value="{{ $key->nama }}" id=""><br>
                    <input type="text" class="form-control" name="username" value="{{ $key->username }}" id=""><br>
                    <input type="text" class="form-control" name="email" value="{{ $key->email }}" id=""><br>
                    <input type="text" class="form-control" name="password" placeholder="Ubah Password" id=""><br>
                    <input type="text" class="form-control" name="nomor_telepon" value="{{ $key->nomor_telepon }}" id=""><br>
                </div>
                <div class="col-lg-6 col-md-12 col-12">
                    <input type="text" class="form-control" name="tahun_lulus" value="{{ $key->tahun_lulus }}" id=""><br>
                    <input type="text" class="form-control" name="asal_universitas" value="{{ $key->asal_universitas }}"><br>
                    <input type="text" class="form-control" name="tempat_praktik" value="{{ $key->tempat_praktik }}"><br>
                    <input type="text" class="form-control" name="nomor_str" value="{{ $key->nomor_str }}"><br>
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12">
                            <input type="file" class="form-control" name="foto" id="">
                        </div>
                        <div class="col-lg-6 col-md-12 col-12">
                            <input type="file" class="form-control" name="file_str" id="">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <center><button class="btn btn-primary btn-lg mt-4">Simpan</button>
                    </center>
                </div>
            </div>
            </form>
            <!-- Row end -->
        </div>
        <!-- Content wrapper end -->


    </div>
@endsection
