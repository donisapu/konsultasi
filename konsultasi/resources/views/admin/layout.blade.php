<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap Dashboards">
    <meta name="author" content="Bootstrap Gallery">
    <link rel="shortcut icon" href="{{ asset('dash/img/favicon.svg') }}" />

    <!-- Title -->
    <title>{{ $title }}</title>


    <!-- *************
   ************ Common Css Files *************
   ************ -->
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="{{ asset('dash/css/bootstrap.min.css') }}">

    <!-- Icomoon Font Icons css -->
    <link rel="stylesheet" href="{{ asset('dash/fonts/style.css') }}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('dash/css/main.min.css') }}">

    <link rel="stylesheet" href="{{ asset('dash/vendor/datepicker/css/classic.css') }}" />
    <link rel="stylesheet" href="{{ asset('dash/vendor/datepicker/css/classic.date.css') }}" />
    <!-- Data Tables -->
		<link rel="stylesheet" href="{{ asset('dash/vendor/datatables/dataTables.bs4.css') }}" />
		<link rel="stylesheet" href="{{ asset('dash/vendor/datatables/dataTables.bs4-custom.css') }}" />


    <!-- *************
   ************ Vendor Css Files *************
  ************ -->
    <style>
        @media (max-width: 767px) {
            .abc {
                max-width: 100%;
            }
        }
    </style>
</head>

<body>

    <!-- Loading starts -->
    {{-- <div id="loading-wrapper">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div> --}}
    <!-- Loading ends -->

    <!-- Header start -->
    <header class="header">
        <div class="container-fluid">

            <!-- Row start -->
            <div class="row gutters">
                <div class="col-sm-4 col-4">
                    <a href="index.html" class="logo">Konsultasi Kita</span></a>
                </div>
                <div class="col-sm-8 col-8">

                    <!-- Header actions start -->
                    <ul class="header-actions">
                        <li class="dropdown">
                            <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                                aria-haspopup="true">
                                <span class="user-name">{{ session()->get('nama') }}</span>
                                <span class="avatar">ADM<span class="status online"></span></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userSettings">
                                <div class="header-profile-actions">

                                    <form action="{{ '/logout/admin' }}" method="post">
                                        @csrf
                                        <a href="{{ '/logout/admin' }}" type="submit"><i class="icon-log-out1"></i> Sign Out</a>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- Header actions end -->

                </div>
            </div>
            <!-- Row end -->

        </div>
    </header>
    <div class="container-fluid">


        <!-- Navigation start -->
        <nav class="navbar navbar-expand-lg custom-navbar">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#royalHospitalsNavbar"
                aria-controls="royalHospitalsNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="royalHospitalsNavbar">
                <ul class="navbar-nav" style="align-content: center">
                    <li class="nav-item">
                        <a class="nav-link active-page" href="{{ '/admin/dashboard' }}">
                            <i class="icon-devices_other nav-icon"></i>
                            Dashboard
                        </a>
                    </li>

                </ul>
            </div>
        </nav>

        @include('sweetalert::alert')
        @yield('container')

        <footer class="main-footer">© </footer>

    </div>

    <!-- *************
   ************ Required JavaScript Files *************
  ************* -->
    <!-- Required jQuery first, then Bootstrap Bundle JS -->
    <script src="{{ asset('dash/js/jquery.min.js') }}"></script>
    <script src="{{ asset('dash/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dash/js/moment.js') }}"></script>


    <!-- *************
   ************ Vendor Js Files *************
  ************* -->

    <!-- Apex Charts -->
    <script src="{{ asset('dash/vendor/apex/apexcharts.min.js') }}"></script>
    <script src="{{ asset('dash/vendor/apex/examples/mixed/hospital-line-column-graph.js') }}"></script>
    <script src="{{ asset('dash/vendor/apex/examples/mixed/hospital-line-area-graph.js') }}"></script>
    <script src="{{ asset('dash/vendor/apex/examples/bar/hospital-patients-by-age.js') }}"></script>

    <!-- Rating JS -->
    <script src="{{ asset('dash/vendor/rating/raty.js') }}"></script>
    <script src="{{ asset('dash/vendor/rating/raty-custom.js') }}"></script>

    <!-- Datepickers -->
    <script src="{{ asset('dash/vendor/datepicker/js/picker.js') }}"></script>
    <script src="{{ asset('dash/vendor/datepicker/js/picker.date.js') }}"></script>
    <script src="{{ asset('dash/vendor/datepicker/js/custom-picker.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Data Tables -->
		<script src="{{ asset('dash/vendor/datatables/dataTables.min.js') }}"></script>
		<script src="{{ asset('dash/vendor/datatables/dataTables.bootstrap.min.js') }}"></script>

		<!-- Custom Data tables -->
		<script src="{{ asset('dash/vendor/datatables/custom/custom-datatables.js') }}"></script>
		<script src="{{ asset('dash/vendor/datatables/custom/fixedHeader.js') }}"></script>

    <!-- Main Js Required -->
    <script src="{{ asset('dash/js/main.js') }}"></script>
    <script>
        // Initialization for ES Users
        import {
            Ripple,
            initMDB
        } from "mdb-ui-kit";

        initMDB({
            Ripple
        });
    </script>
    @if (Session::has('success'))
        <script src="{{ asset('sweetalert/sweet-alert.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil',
                    text: '{{ Session::get('success') }}',
                });
            });
        </script>
    @endif
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>
        $(document).on('click', '.hapus-data', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');

            Swal.fire({
                title: 'Konfirmasi',
                text: 'Apakah Anda yakin ingin menghapus data ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Redirect to the delete URL
                    window.location.href = url;
                }
            });
        });
    </script>
    <script>
        $(document).on('click', '.terima-data', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');

            Swal.fire({
                title: 'Konfirmasi',
                text: 'Apakah anda ingin Menerima Psikolog ini ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Terima',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Redirect to the delete URL
                    window.location.href = url;
                }
            });
        });
    </script>
    <script>
        $(document).on('click', '.tolak-data', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');

            Swal.fire({
                title: 'Konfirmasi',
                text: 'Apakah anda ingin Menolak Psikolog ini ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Tolak',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Redirect to the delete URL
                    window.location.href = url;
                }
            });
        });
    </script>

</body>

</html>
