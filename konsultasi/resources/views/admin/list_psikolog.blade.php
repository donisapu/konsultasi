@extends('admin.layout')
@section('container')

        <!-- Navigation end -->


        <!-- *************
        ************ Main container start *************
       ************* -->
        <div class="main-container">


            <!-- Page header start -->

            <!-- Page header end -->

            <!-- Content wrapper start -->
            <div class="content-wrapper">

                <!-- Row start -->
                @if (count($psikolog) > 0)
                <div class="row gutters">
                    @foreach ($psikolog as $key)
                    <div class="col-lg-12 col-md-12 col-12">
                        <a href="{{ '/admin/verifikasi/'.$key->id }}" class="hospital-list">
                            <img src="{{ asset('/psikolog/foto/'.$key->foto) }}" class="hospital-thumb" alt="Medical Dashboards" />
                            <div class="hospital-details">
                                <div class="hospital-location">
                                    <h4>{{ $key->nama }}</h4>
                                    <h5>{{ $key->nomor_str }}</h5>
                                    <h5>{{ $key->nomor_telepon }}</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                @else
                <div class="row gutters">
                    <div class="col-12">
                        <h3>Belum ada psikolog yang mendaftar</h3>
                    </div>
                </div>
                @endif

                <!-- Row end -->
            </div>
            <!-- Content wrapper end -->


        </div>
    @endsection
